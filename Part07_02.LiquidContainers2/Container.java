
public class Container {
        private int total;
        
    public Container() {
        
    }
    
    public int contains() {
        return total;
    }
    
    public void add(int amount) {
        if (amount > 0) {
        total += amount;
        }
        
        if (total > 100) {
            total = 100;
        }
    }
    
    public void remove(int amount) {
        if (amount > 0) {
            total -= amount;
        }
        
        if (total < 0) {
            total = 0;
        }
    }
    
    public String toString() {
        return total + "/100";
    }
}
