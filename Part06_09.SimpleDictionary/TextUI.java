import java.util.Scanner;

public class TextUI {

    private Scanner scanner;
    private SimpleDictionary simpleDictionary;

    public TextUI(Scanner scanner, SimpleDictionary simpleDictionary) {
        this.scanner = scanner;
        this.simpleDictionary = simpleDictionary;
    }

    public void start() {

        while (true) {
            System.out.println("Command: ");
            String command = scanner.nextLine();

            if (command.contains("end")) {
                System.out.println("Bye bye!");
                break;
            }

            if (command.contains("add")) {
                System.out.println("Word: ");
                String word = scanner.nextLine();
                System.out.println("Translation: ");
                String translation = scanner.nextLine();
                this.simpleDictionary.add(word, translation);
            }

            if (command.contains("search")) {
                System.out.println("To be translated:");
                String preTranslation = scanner.nextLine();
                if (this.simpleDictionary.translate(preTranslation) == null) {
                    System.out.println("Word " + preTranslation + " was not found");
                } else {
                    System.out.println("Translation: " + this.simpleDictionary.translate(preTranslation));
                }
                
            } else {
                System.out.println("Unknown command");
            }
        }
    }
}
