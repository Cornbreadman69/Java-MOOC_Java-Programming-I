public class SimpleDate {

    private int day;
    private int month;
    private int year;

    public SimpleDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public String toString() {
        return this.day + "." + this.month + "." + this.year;
    }

    public boolean before(SimpleDate compared) {
        // first compare years
        if (this.year < compared.year) {
            return true;
        }

        // if the years are the same, compare months
        if (this.year == compared.year && this.month < compared.month) {
            return true;
        }

        // the years and the months are the same, compare days
        if (this.year == compared.year && this.month == compared.month
                && this.day < compared.day) {
            return true;
        }

        return false;
    }

    public void advance() {
        if (this.month == 12 && this.day == 30) {
            this.day = 0;
            this.month = 1;
            this.year = this.year + 1;
        }

        if (this.month != 12 && this.day == 30) {
            this.day = 0;
            this.month = this.month + 1;
        }

        if (this.day != 30) {
            this.day = this.day + 1;
        }
    }
    
//	I liked the challenge of creating an advance method with realistic parameters. The method is shown below:      
    public void trueAdvance() {
        if (this.month == 2) {
            if (this.year % 4 == 0) {
                if ((this.year % 100 == 0 && this.year % 400 == 0)) {
                    if (this.day < 29) {
                        this.day = this.day + 1;
                    } else {
                        this.month = 3;
                        this.day = 0;
                    }

                } else if ((this.year % 100 == 0) && !(this.year % 400 == 0)) {
                    if (this.day < 28) {
                        this.day = this.day + 1;
                    } else {
                        this.month = 3;
                        this.day = 0;
                    }

                } else {
                    if (this.day < 29) {
                        this.day = this.day + 1;
                    } else {
                        this.month = 3;
                        this.day = 0;
                    }
                }
            }

            if (this.year % 4 != 0) {
                if (this.day < 28) {
                    this.day = this.day + 1;
                } else {
                    this.month = 3;
                    this.day = 0;
                }
            }
        }

        if (this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11) {
            if (this.day < 30) {
                this.day = this.day + 1;
            } else {
                this.month = this.month + 1;
                this.day = 0;
            }
        }

        if (this.month == 1 || this.month == 3 || this.month == 5 || this.month == 7 || this.month == 8 || this.month == 10) {
            if (this.day < 31) {
                this.day = this.day + 1;
            } else {
                this.month = this.month + 1;
                this.day = 1;
            }
        }

        if (this.month == 12) {
            if (this.day < 31) {
                this.day = this.day + 1;
            } else {
                this.month = 1;
                this.day = 1;
                this.year = this.year + 1;
            }
        }
    }
    
    
    public void advance(int howManyDays) {
        for (int i = 0; i < howManyDays; i++) {
            advance();
        }
    }

    public SimpleDate afterNumberOfDays(int days) {
        SimpleDate newDate = new SimpleDate(this.day, this.month, this.year);
        newDate.advance(days);
        return newDate;
    }
}
