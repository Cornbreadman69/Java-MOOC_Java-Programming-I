
public class Archive {

    private String identifier;
    private String name;

    public Archive(String identifier, String name) {
        this.identifier = identifier;
        this.name = name;
    }

    public Archive(String identifier) {
        this(identifier, "");
    }

    public boolean equals(Object compared) {
        if (this == compared) {
            return true;
        }

        if (!(compared instanceof Archive)) {
            return false;
        }

        Archive other = (Archive) compared;

        return this.identifier.equals(other.identifier);
    }

    public String toString() {
        return this.identifier + ": " + this.name;
    }
}
