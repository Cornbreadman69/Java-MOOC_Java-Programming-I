# Java-Mooc

My answers to the Helsinki Java Mooc.fi problems This is my first experience working with java and OOP. Note: according to their documentation on https://materiaalit.github.io/2013-oo-programming/part1/week-1/ the following license was given. I want to give credit where credit is due:

This material is licensed under the Creative Commons BY-NC-SA license, which means that you can use it and distribute it freely so long as you do not erase the names of the original authors. If you do changes in the material and want to distribute this altered version of the material, you have to license it with a similar free license. The use of the material for commercial use is prohibited without a separate agreement.

Authors: Arto Hellas, Matti Luukkainen
Translators to English: Emilia Hjelm, Alex H. Virtanen, Matti Luukkainen, Virpi Sumu, Birunthan Mohanathas

The course is maintained by the Agile Education Research Group.
