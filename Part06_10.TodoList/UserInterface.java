import java.util.Scanner;

public class UserInterface {
    private TodoList todoList;
    private Scanner scanner;
    
    public UserInterface(TodoList todoList, Scanner scanner){
        this.todoList = todoList;
        this.scanner = scanner;
    }
    
    public void start(){
        while (true){
            System.out.println("Command: ");
            String command = scanner.nextLine();
            
            if (command.equals("stop")){
                break;
            }
            
            if (command.equals("add")){
                System.out.println("To add: ");
                String toBeAdded = scanner.nextLine();
                this.todoList.add(toBeAdded);
            }
            
            if (command.equals("list")){
                this.todoList.print();
            }
            
            if (command.equals("remove")){
                System.out.println("Which one is removed?");
                int removed = Integer.valueOf(scanner.nextLine());
                this.todoList.remove(removed);
            }
        }
    }
}
